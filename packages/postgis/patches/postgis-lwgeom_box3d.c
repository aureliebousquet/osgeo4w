diff --git postgis/lwgeom_box3d.c postgis/lwgeom_box3d.c
index 9a34500..328a794 100644
--- postgis/lwgeom_box3d.c
+++ postgis/lwgeom_box3d.c
@@ -605,7 +605,7 @@ BOX3D_contains_internal(BOX3D *box1, BOX3D *box2)
 
 PG_FUNCTION_INFO_V1(BOX3D_contains);
 
-PGDLLEXPORT Datum BOX3D_contains(PG_FUNCTION_ARGS)
+Datum BOX3D_contains(PG_FUNCTION_ARGS)
 {
 	BOX3D *box1 = PG_GETARG_BOX3D_P(0);
 	BOX3D *box2 = PG_GETARG_BOX3D_P(1);
@@ -622,7 +622,7 @@ BOX3D_contained_internal(BOX3D *box1, BOX3D *box2)
 
 PG_FUNCTION_INFO_V1(BOX3D_contained);
 
-PGDLLEXPORT Datum BOX3D_contained(PG_FUNCTION_ARGS)
+Datum BOX3D_contained(PG_FUNCTION_ARGS)
 {
 	BOX3D *box1 = PG_GETARG_BOX3D_P(0);
 	BOX3D *box2 = PG_GETARG_BOX3D_P(1);
@@ -641,7 +641,7 @@ BOX3D_overlaps_internal(BOX3D *box1, BOX3D *box2)
 
 PG_FUNCTION_INFO_V1(BOX3D_overlaps);
 
-PGDLLEXPORT Datum BOX3D_overlaps(PG_FUNCTION_ARGS)
+Datum BOX3D_overlaps(PG_FUNCTION_ARGS)
 {
 	BOX3D *box1 = PG_GETARG_BOX3D_P(0);
 	BOX3D *box2 = PG_GETARG_BOX3D_P(1);
@@ -660,7 +660,7 @@ BOX3D_same_internal(BOX3D *box1, BOX3D *box2)
 
 PG_FUNCTION_INFO_V1(BOX3D_same);
 
-PGDLLEXPORT Datum BOX3D_same(PG_FUNCTION_ARGS)
+Datum BOX3D_same(PG_FUNCTION_ARGS)
 {
 	BOX3D *box1 = PG_GETARG_BOX3D_P(0);
 	BOX3D *box2 = PG_GETARG_BOX3D_P(1);
@@ -677,7 +677,7 @@ BOX3D_left_internal(BOX3D *box1, BOX3D *box2)
 
 PG_FUNCTION_INFO_V1(BOX3D_left);
 
-PGDLLEXPORT Datum BOX3D_left(PG_FUNCTION_ARGS)
+Datum BOX3D_left(PG_FUNCTION_ARGS)
 {
 	BOX3D *box1 = PG_GETARG_BOX3D_P(0);
 	BOX3D *box2 = PG_GETARG_BOX3D_P(1);
@@ -694,7 +694,7 @@ BOX3D_overleft_internal(BOX3D *box1, BOX3D *box2)
 
 PG_FUNCTION_INFO_V1(BOX3D_overleft);
 
-PGDLLEXPORT Datum BOX3D_overleft(PG_FUNCTION_ARGS)
+Datum BOX3D_overleft(PG_FUNCTION_ARGS)
 {
 	BOX3D *box1 = PG_GETARG_BOX3D_P(0);
 	BOX3D *box2 = PG_GETARG_BOX3D_P(1);
@@ -711,7 +711,7 @@ BOX3D_right_internal(BOX3D *box1, BOX3D *box2)
 
 PG_FUNCTION_INFO_V1(BOX3D_right);
 
-PGDLLEXPORT Datum BOX3D_right(PG_FUNCTION_ARGS)
+Datum BOX3D_right(PG_FUNCTION_ARGS)
 {
 	BOX3D *box1 = PG_GETARG_BOX3D_P(0);
 	BOX3D *box2 = PG_GETARG_BOX3D_P(1);
@@ -728,7 +728,7 @@ BOX3D_overright_internal(BOX3D *box1, BOX3D *box2)
 
 PG_FUNCTION_INFO_V1(BOX3D_overright);
 
-PGDLLEXPORT Datum BOX3D_overright(PG_FUNCTION_ARGS)
+Datum BOX3D_overright(PG_FUNCTION_ARGS)
 {
 	BOX3D *box1 = PG_GETARG_BOX3D_P(0);
 	BOX3D *box2 = PG_GETARG_BOX3D_P(1);
@@ -745,7 +745,7 @@ BOX3D_below_internal(BOX3D *box1, BOX3D *box2)
 
 PG_FUNCTION_INFO_V1(BOX3D_below);
 
-PGDLLEXPORT Datum BOX3D_below(PG_FUNCTION_ARGS)
+Datum BOX3D_below(PG_FUNCTION_ARGS)
 {
 	BOX3D *box1 = PG_GETARG_BOX3D_P(0);
 	BOX3D *box2 = PG_GETARG_BOX3D_P(1);
@@ -762,7 +762,7 @@ BOX3D_overbelow_internal(BOX3D *box1, BOX3D *box2)
 
 PG_FUNCTION_INFO_V1(BOX3D_overbelow);
 
-PGDLLEXPORT Datum BOX3D_overbelow(PG_FUNCTION_ARGS)
+Datum BOX3D_overbelow(PG_FUNCTION_ARGS)
 {
 	BOX3D *box1 = PG_GETARG_BOX3D_P(0);
 	BOX3D *box2 = PG_GETARG_BOX3D_P(1);
@@ -779,7 +779,7 @@ BOX3D_above_internal(BOX3D *box1, BOX3D *box2)
 
 PG_FUNCTION_INFO_V1(BOX3D_above);
 
-PGDLLEXPORT Datum BOX3D_above(PG_FUNCTION_ARGS)
+Datum BOX3D_above(PG_FUNCTION_ARGS)
 {
 	BOX3D *box1 = PG_GETARG_BOX3D_P(0);
 	BOX3D *box2 = PG_GETARG_BOX3D_P(1);
@@ -796,7 +796,7 @@ BOX3D_overabove_internal(BOX3D *box1, BOX3D *box2)
 
 PG_FUNCTION_INFO_V1(BOX3D_overabove);
 
-PGDLLEXPORT Datum BOX3D_overabove(PG_FUNCTION_ARGS)
+Datum BOX3D_overabove(PG_FUNCTION_ARGS)
 {
 	BOX3D *box1 = PG_GETARG_BOX3D_P(0);
 	BOX3D *box2 = PG_GETARG_BOX3D_P(1);
@@ -813,7 +813,7 @@ BOX3D_front_internal(BOX3D *box1, BOX3D *box2)
 
 PG_FUNCTION_INFO_V1(BOX3D_front);
 
-PGDLLEXPORT Datum BOX3D_front(PG_FUNCTION_ARGS)
+Datum BOX3D_front(PG_FUNCTION_ARGS)
 {
 	BOX3D *box1 = PG_GETARG_BOX3D_P(0);
 	BOX3D *box2 = PG_GETARG_BOX3D_P(1);
@@ -830,7 +830,7 @@ BOX3D_overfront_internal(BOX3D *box1, BOX3D *box2)
 
 PG_FUNCTION_INFO_V1(BOX3D_overfront);
 
-PGDLLEXPORT Datum BOX3D_overfront(PG_FUNCTION_ARGS)
+Datum BOX3D_overfront(PG_FUNCTION_ARGS)
 {
 	BOX3D *box1 = PG_GETARG_BOX3D_P(0);
 	BOX3D *box2 = PG_GETARG_BOX3D_P(1);
@@ -847,7 +847,7 @@ BOX3D_back_internal(BOX3D *box1, BOX3D *box2)
 
 PG_FUNCTION_INFO_V1(BOX3D_back);
 
-PGDLLEXPORT Datum BOX3D_back(PG_FUNCTION_ARGS)
+Datum BOX3D_back(PG_FUNCTION_ARGS)
 {
 	BOX3D *box1 = PG_GETARG_BOX3D_P(0);
 	BOX3D *box2 = PG_GETARG_BOX3D_P(1);
@@ -864,7 +864,7 @@ BOX3D_overback_internal(BOX3D *box1, BOX3D *box2)
 
 PG_FUNCTION_INFO_V1(BOX3D_overback);
 
-PGDLLEXPORT Datum BOX3D_overback(PG_FUNCTION_ARGS)
+Datum BOX3D_overback(PG_FUNCTION_ARGS)
 {
 	BOX3D *box1 = PG_GETARG_BOX3D_P(0);
 	BOX3D *box2 = PG_GETARG_BOX3D_P(1);
@@ -921,7 +921,7 @@ BOX3D_distance_internal(BOX3D *box1, BOX3D *box2)
 
 PG_FUNCTION_INFO_V1(BOX3D_distance);
 
-PGDLLEXPORT Datum BOX3D_distance(PG_FUNCTION_ARGS)
+Datum BOX3D_distance(PG_FUNCTION_ARGS)
 {
 	BOX3D *box1 = PG_GETARG_BOX3D_P(0);
 	BOX3D *box2 = PG_GETARG_BOX3D_P(1);
