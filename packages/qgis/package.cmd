::--------- Package settings --------
:: package name
set P=qgis-oslandia
:: version
set V=3.13.0
:: package version
set B=1

set HERE=%CD%

::--------- Prepare the environment
set BUILD_DEPS=qgis-dev-deps

call ..\__inc__\prepare_env.bat %1
set OSGEO4W_ROOT=c:\osgeo4w64
set PATH=%OSGEO4W_ROOT%\bin;%PATH%
set PYTHONPATH=%PYTHONPATH%;%HERE%
set INCLUDE=%OSGEO4W_ROOT%\include

git clone https://github.com/troopa81/QGIS.git --depth 1 --branch fix_package_cmd || goto :error

:: if we build on local we force generator to VisualStudio so we can debug
:: Actually, it will fail because of limitation on Path length on Windows
:: but the sln can be opened in Visual Studio in order to be built
if "%1"=="local" (
	set CMAKEGEN=Visual Studio 15 2017 Win64
)

set ARCH=x86_64
cd QGIS/ms-windows/osgeo4w/

touch skiptests
call package.cmd %V% %B% %P% x86_64

::--------- Installation
scp %PKG_BIN% %R% || goto :error
scp setup.hint %R% || goto :error
goto :EOF


:error
echo Build failed
exit /b 1
